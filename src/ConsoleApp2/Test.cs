﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Test
    {

        private string fname;
        private string lname;

        public string getFname()
        {
            return fname;
        }

        public void setFname(string type)
        {
            fname = type;
        }


        public string getLname()
        {
            return lname;
        }

        public void setLname(string type)
        {
            lname = type;
        }
        public Test()
        {
            Console.WriteLine("Creating person object from default constructor (accepts no arguments): ");
            fname = "John";
            lname = "Doe";
            Console.WriteLine("First Name: " + this.fname);
            Console.WriteLine("Last Name: " + this.lname);
            Console.WriteLine();
        }
        public Test(string fn, string ln)
        {
            fname = fn;
            lname = ln;
            Console.WriteLine("Call parameterized construtor (accepts two arguments): ");
            Console.WriteLine("Full Name: " + this.fname + " " + this.lname);
        }

    }
}
