﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;


namespace ConsoleApp2
{

    class Program
    {



        static void Main(string[] args)
        {

            Console.WriteLine("P1: Room Calculator");
            Console.WriteLine("Author: Cuong Huynh");
            Console.WriteLine("Now: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));
            Console.WriteLine("    ");

            //USING DEFAULT CONS.
            Test defaultConstructor = new Test();


            //USING SET/GET



            Console.WriteLine("Modify default constructor object's data member values: ");
            Console.WriteLine("User setter/getter method: ");
            Console.Write("First Name: ");
            string setterFname = Console.ReadLine();
            defaultConstructor.setFname(setterFname);


            Console.Write("Last Name: ");
            string setterLname = Console.ReadLine();
            defaultConstructor.setLname(setterLname);

            //DISPLAY SET/GET


            Console.WriteLine();
            Console.WriteLine("Display object's new data member values: ");


            Console.Write("First Name: ");
            Console.WriteLine(defaultConstructor.getFname());


            Console.Write("Last Name: ");
            Console.WriteLine(defaultConstructor.getLname());
            Console.WriteLine();

            //USING PARAMETERIZED CONS.
            Console.WriteLine("Call parameterized construtor (accepts two arguments): ");


            Console.Write("First Name: ");
            string firstName = Console.ReadLine();


            Console.Write("Last Name: ");
            string lastName = Console.ReadLine();
            Console.WriteLine();


            Test paramConstructor = new Test(firstName, lastName);

            Console.WriteLine("  ");
            Console.WriteLine("Press Enter to exit ...");
            Console.WriteLine("  ");
            Console.ReadKey(true);

        }
    }
}
